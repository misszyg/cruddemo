package com.yzg.cruddemo.resp;

import java.util.List;

public class PageResp<T> {

    private Long total;//总数

    private List<T> list;   // 数据

    public PageResp() {
    }

    public PageResp(Long total, List<T> list) {
        this.total = total;
        this.list = list;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "PageResp{" +
                "total=" + total +
                ", list=" + list +
                '}';
    }
}
