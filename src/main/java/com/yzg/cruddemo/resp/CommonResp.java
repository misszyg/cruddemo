package com.yzg.cruddemo.resp;

public class CommonResp <T>{
    //业务上的成功与失败
    private Boolean success = true;
    //放回信息
    private String message;
    //返回泛型数据 自定义类型
    private T content;

    public CommonResp() {
    }

    public CommonResp(Boolean success, String message, T content) {
        this.success = success;
        this.message = message;
        this.content = content;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "CommonResp{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", content=" + content +
                '}';
    }
}
