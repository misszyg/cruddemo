package com.yzg.cruddemo.config;

/**
 * @BelongsProject: cruddemo
 * @BelongsPackage: com.yzg.cruddemo.config
 * @Author: YangZhenGuang1
 * @Date: 2022/9/16 10:55
 * @Description: TODO
 */
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Value("${file.staticPatternPath}")
    private String staticPatternPath;
    @Value("${file.uploadFolder}")
    private String uploadFolder;
    //springboot 中mvc 让程序开发者 配置文件上传的额外的静态资源服务的配置
    /*
     * /uploadimg 访问的路径
     * file:F://tmp  上传资源的路径 随便写哪 （服务器 或者 本机盘 ）
     *
     * 这个时候 把aaa.jpg上传到F://tmp下面，我们就能通过 http://localhost:8777/uploadimg/aaa/jpg 访问
     *
     * 可以得知  这个是用来映射的
     * */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/uploadimg/**").addResourceLocations("file:D://tmp/");
        registry.addResourceHandler(staticPatternPath).addResourceLocations("file:"+uploadFolder);
    }
}

