package com.yzg.cruddemo.controller;

/**
 * @BelongsProject: cruddemo
 * @BelongsPackage: com.yzg.cruddemo.controller
 * @Author: YangZhenGuang1
 * @Date: 2022/9/16 10:56
 * @Description: TODO
 */
import com.yzg.cruddemo.entity.UserEntity;
import com.yzg.cruddemo.req.UserSaveReq;
import com.yzg.cruddemo.resp.CommonResp;
import com.yzg.cruddemo.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RestController
public class UploadController {
    @Autowired
    private UploadService uploadservice;
    @PostMapping("/upload/file")
    public CommonResp upload(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request){
        CommonResp<String> resp=new CommonResp<>();
        if(multipartFile.isEmpty()){
            resp.setMessage("文件有误");
            return resp;
        }
        long size= multipartFile.getSize(); //获得大小
        String filename = multipartFile.getOriginalFilename(); //获得名字 最开始的名字
        System.out.println(filename);
        String contentType = multipartFile.getContentType(); //获得类型 后面可以进行筛选
        //选择图片的正则表达式
//        if(!contentType.equals("png|jpg")){
//            return "类型不正确";
//        }

        //1.获取用户指定的文件夹 为什么要从页面上传过来呢？
        //原因 做隔离 不同的业务 放入不同的文件或者图片。 防止混乱
        String dir = request.getParameter("dir");
        String s = uploadservice.uploadImg(multipartFile, dir);
        System.out.println("Fileload===============s>"+ s);
        //下面就要存储到数据库1了 先执行增加操作 把图片信息添加进行
        UserEntity userEntity=new UserEntity();
        userEntity.setSex(s);
        resp.setContent(s);
        return resp;
    }



    /*
    *  @PostMapping("/upload/file")
    public String upload(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request){
//        CommonResp resp=new CommonResp();
        if(multipartFile.isEmpty()){
            return  "文件有误";
        }
        long size= multipartFile.getSize(); //获得大小
        String filename = multipartFile.getOriginalFilename(); //获得名字 最开始的名字
        System.out.println(filename);
        String contentType = multipartFile.getContentType(); //获得类型 后面可以进行筛选
        //选择图片的正则表达式
//        if(!contentType.equals("png|jpg")){
//            return "类型不正确";
//        }

        //1.获取用户指定的文件夹 为什么要从页面上传过来呢？
        //原因 做隔离 不同的业务 放入不同的文件或者图片。 防止混乱
        String dir = request.getParameter("dir");
        return uploadservice.uploadImg(multipartFile,dir);
    }
    *
    *
    * */





    @PostMapping("/upload2/file2")
    public Map<String,Object> uploadMap(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request){
        if(multipartFile.isEmpty()){
            return  null;
        }
        long size= multipartFile.getSize(); //获得大小
        String filename = multipartFile.getOriginalFilename(); //获得名字 最开始的名字
        System.out.println(filename);
        String contentType = multipartFile.getContentType(); //获得类型 后面可以进行筛选
        //选择图片的正则表达式
//        if(!contentType.equals("png|jpg")){
//            return "类型不正确";
//        }

        //1.获取用户指定的文件夹 为什么要从页面上传过来呢？
        //原因 做隔离 不同的业务 放入不同的文件或者图片。 防止混乱
        String dir = request.getParameter("dir");
        return uploadservice.uploadImgMap(multipartFile,dir);
    }
}
