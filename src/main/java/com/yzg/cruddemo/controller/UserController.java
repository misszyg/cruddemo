package com.yzg.cruddemo.controller;

import com.yzg.cruddemo.entity.UserEntity;
import com.yzg.cruddemo.req.UserReq;
import com.yzg.cruddemo.req.UserSaveReq;
import com.yzg.cruddemo.resp.CommonResp;
import com.yzg.cruddemo.resp.PageResp;
import com.yzg.cruddemo.service.UploadService;
import com.yzg.cruddemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UploadService uploadservice;

    @PostMapping("/upload/file")
    public String upload(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request){
        if(multipartFile.isEmpty()){
            return "文件有误";
        }
        //long size= multipartFile.getSize(); //获得大小
        String filename = multipartFile.getOriginalFilename(); //获得名字 最开始的名字
        System.out.println("filename---------------------->"+filename);
        //String contentType = multipartFile.getContentType(); //获得类型 后面可以进行筛选
        //选择图片的正则表达式
//        if(!contentType.equals("png|jpg")){
//            return "类型不正确";
//        }

        //1.获取用户指定的文件夹 为什么要从页面上传过来呢？
        //原因 做隔离 不同的业务 放入不同的文件或者图片。 防止混乱
        String dir = request.getParameter("dir");
        String s = uploadservice.uploadImg(multipartFile, dir);
        System.out.println("Fileload===============s>"+ s);

        return s;
    }

    @GetMapping("/getList")
    public CommonResp getList(UserReq userReq){
        // 以为调用了这个CommonResp工具类 把需要查找的 实体类集合 放进去。  resp需要返回给前端
        CommonResp<PageResp<UserEntity>> resp = new CommonResp<>(); //PageResp 还实现了 分页
        PageResp<UserEntity> list=userService.getList(userReq); //userReq 根据 名字 电话 城市 查找。

        resp.setContent(list);   // 集合放入 之定义类型中
        resp.setMessage("getList查询全部的方法");
        System.out.println("getList查询全部的方法"+resp);
        return resp;
    }
    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id){ //接收id
        CommonResp<UserEntity> resp = new CommonResp<>();
        userService.delete(id);
        resp.setMessage("delete删除方法");
        return resp;
    }
    @PostMapping("/save")
    public CommonResp save(UserSaveReq req,@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request){ //不操作实体类的方法.保证数据的一致性
        CommonResp<UserSaveReq> resp = new CommonResp<>();
        System.out.println(resp);
        if(multipartFile.isEmpty()){
            resp.setMessage("文件有误");
            return resp;
        }
        long size= multipartFile.getSize(); //获得大小
        String filename = multipartFile.getOriginalFilename(); //获得名字 最开始的名字
        System.out.println("filename---------------------->"+filename);
        //String contentType = multipartFile.getContentType(); //获得类型 后面可以进行筛选
        //选择图片的正则表达式
//        if(!contentType.equals("png|jpg")){
//            return "类型不正确";
//        }

        //1.获取用户指定的文件夹 为什么要从页面上传过来呢？
        //原因 做隔离 不同的业务 放入不同的文件或者图片。 防止混乱
        String dir = request.getParameter("dir");
        String s = uploadservice.uploadImg(multipartFile, dir);
        System.out.println("Fileload===============s>"+ s);
        //下面就要存储到数据库1了 先执行增加操作 把图片信息添加进行

        req.setSex(s); // 文件确实上传成功了 但是。 直接调用了这里的方法 就只加了和 图片地址进去
        userService.save(req);
        System.out.println("新增方法");
        resp.setContent(req);
        return resp;
    }



}
