package com.yzg.cruddemo.req;

public class PageReq { //类
    //页码
    private int page;
    //每页条数
    private int size;

    public PageReq() {
    }

    public PageReq(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "PageReq{" +
                "page=" + page +
                ", size=" + size +
                '}';
    }
}
