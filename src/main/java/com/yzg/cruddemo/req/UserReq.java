package com.yzg.cruddemo.req;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserReq extends PageReq{ //子类
   // private Long id;
    private String name;
   //private int age;
  // private String sex;
    private String phone;
    private String city;


}
