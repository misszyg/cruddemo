package com.yzg.cruddemo.service;

/**
 * @BelongsProject: cruddemo
 * @BelongsPackage: com.yzg.cruddemo.service
 * @Author: YangZhenGuang1
 * @Date: 2022/9/16 10:57
 * @Description: TODO
 */
import com.yzg.cruddemo.entity.UserEntity;
import com.yzg.cruddemo.mapper.UserMapper;
import com.yzg.cruddemo.req.UserSaveReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * MultipartFile 这个对象是springmvc提供的文件上传的接收类
 * 他的底层会自动和httpservletRequest request中的request.getInputStream()融合
 * 从而实现文件上传。底层原理就是  request.getInputStream()
 */

@Service
public class UploadService {
    @Value("${file.staticPatternPath}")
    private String staticPatternPath; // 访问的路径
    @Value("${file.uploadFolder}")
    private String uploadFolder; //存储地址
    @Value("${file.staticPath}")
    private String staticPath; //域名地址
    @Autowired
    private UserMapper userMapper;

    public String uploadImg(MultipartFile multipartFile,String dir){

        try {

            //保证不覆盖文件名
            //1,真实的文件名
            String realName=multipartFile.getOriginalFilename();
            //2, 截取文件名的后缀
            String imgSuffix = realName.substring(realName.lastIndexOf("."));
            //3,生成唯一一个文件名
            String newFilename= UUID.randomUUID()+imgSuffix; //拼接上后缀
            //4, 日期目录
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String dataPath = dateFormat.format(new Date());

            String serverpath=uploadFolder;

            //指定文件上传的目录
            File targetFile = new File(serverpath+dir,dataPath); //dir 自己在指定一个文件目录
            // 如果不存在我就创建一个
            if(!targetFile.exists()){
                targetFile.mkdirs();
            };

            //传入后 把名字给它
            File targetFileName=new File(targetFile,newFilename);
            // 调用内部方法  上传到指定目录
            multipartFile.transferTo(targetFileName);

            //可以访问的路径要返回页面
            String rename = dir+"/"+dataPath+"/"+newFilename;
            //返回的路径信息 直接能访问到图片地址
            String s = staticPath + "/upimages/" + rename;
            System.out.println("s==================>"+s);


            return s;
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
    }

    //返回其他的
    public Map<String,Object> uploadImgMap(MultipartFile multipartFile, String dir){

        try {

            //保证不覆盖文件名
            //1,真实的文件名
            String realName=multipartFile.getOriginalFilename();
            //2, 截取文件名的后缀
            String imgSuffix = realName.substring(realName.lastIndexOf("."));
            //3,生成唯一一个文件名
            String newFilename= UUID.randomUUID()+imgSuffix; //拼接上后缀
            //4, 日期目录
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String dataPath = dateFormat.format(new Date());

            String serverpath=uploadFolder;

            //指定文件上传的目录
            File targetFile = new File(serverpath+dir,dataPath); //dir 自己在指定一个文件目录
            // 如果不存在我就创建一个
            if(!targetFile.exists()){
                targetFile.mkdirs();
            };

            //传入后 把名字给它
            File targetFileName=new File(targetFile,newFilename);
            // 调用内部方法  上传到指定目录
            multipartFile.transferTo(targetFileName);

            //可以访问的路径要返回页面
            String rename = dir+"/"+dataPath+"/"+newFilename;
            System.out.println(staticPath+"/upimages/"+rename);

            Map<String,Object> map=new HashMap<>();

            map.put("url",staticPath+"/upimages/"+rename); //直接 url能访问的图片位置
            map.put("size",multipartFile.getSize()); // 大小
            map.put("suffix",imgSuffix); //后缀
            map.put("realfilename",realName);//真实的文件名
            map.put("rpath",dir+"/"+dataPath+"/"+newFilename);//先对路径


            return map;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

