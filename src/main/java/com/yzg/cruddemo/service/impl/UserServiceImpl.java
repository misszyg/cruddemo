package com.yzg.cruddemo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzg.cruddemo.entity.UserEntity;
import com.yzg.cruddemo.mapper.UserMapper;
import com.yzg.cruddemo.req.UserReq;
import com.yzg.cruddemo.req.UserSaveReq;
import com.yzg.cruddemo.resp.CommonResp;
import com.yzg.cruddemo.resp.PageResp;
import com.yzg.cruddemo.service.UserService;
import com.yzg.cruddemo.utils.CopyUtil;
import com.yzg.cruddemo.utils.SnowFlake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SnowFlake snowFlake;
    @Override
    public PageResp<UserEntity> getList(UserReq userReq) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        //条件查询
        if(!ObjectUtils.isEmpty(userReq.getName())){ //名字不为空
            queryWrapper.lambda().eq(UserEntity::getName,userReq.getName());//有值才会掉下面这个
        }
        if(!ObjectUtils.isEmpty(userReq.getPhone())){ //名字不为空
            queryWrapper.lambda().eq(UserEntity::getPhone,userReq.getPhone());//有值才会掉下面这个
        }
        if(!ObjectUtils.isEmpty(userReq.getCity())){ //名字不为空
            queryWrapper.lambda().eq(UserEntity::getCity,userReq.getCity());//有值才会掉下面这个
        }
        //  这里我把 子类继承 父类的 page size方法 放入了mybatis-plus的 page类中
        Page<UserEntity> page = new Page<>(userReq.getPage(), userReq.getSize());

        //mybatis-plus中 的方法 （需要一个 page页码 size每页条数==>page , 查询出来的值）  userMapper.selectPage 自带的方法
        IPage<UserEntity> userEntityIPage = userMapper.selectPage(page, queryWrapper);
        // 在IPage中 获得Toadl总数 和 集合列

        //再通过 自己写的工具类 赋值。获得Toadl总数 和 集合列
        PageResp<UserEntity> pageResp = new PageResp<>();
        pageResp.setTotal(userEntityIPage.getTotal());//Toadl总数
        pageResp.setList(userEntityIPage.getRecords());// 集合列
        return pageResp;
    }

    @Override
    public void delete(Long id) {
       userMapper.deleteById(id);
    }

    @Override
    public void save(UserSaveReq req) {
        UserEntity entity = CopyUtil.copy(req, UserEntity.class);
        if(ObjectUtils.isEmpty(req.getId())){ //id判断是 新增方法 还是更新操作
            entity.setId(snowFlake.nextId()); //给一个id  新增方法  这里 用的是雪花算法  id又臭又长
            System.out.println(entity);
            userMapper.insert(entity);
        }else{
            //更新
            userMapper.updateById(entity);
        }
    }
}
