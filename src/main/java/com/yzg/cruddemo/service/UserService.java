package com.yzg.cruddemo.service;

import com.yzg.cruddemo.entity.UserEntity;
import com.yzg.cruddemo.req.UserReq;
import com.yzg.cruddemo.req.UserSaveReq;
import com.yzg.cruddemo.resp.CommonResp;
import com.yzg.cruddemo.resp.PageResp;

public interface UserService{


    PageResp<UserEntity> getList(UserReq userReq);

    void delete(Long id);

    void save(UserSaveReq req);
}
