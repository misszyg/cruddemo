package com.yzg.cruddemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzg.cruddemo.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
}
